package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh10N052 {
    public static String s = "";

    public static void resetValueOfString() {
        s = "";
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the natural number");
        int n = scanner.nextInt();
        int result = reversePrint(n);
        System.out.println(result);
    }

    public static int reversePrint(int n) {
        if (n > 0) {
            s += "" + n % 10;
            reversePrint(n / 10);
            return Integer.parseInt(s);
        }
        return Integer.parseInt(s);
    }
}
