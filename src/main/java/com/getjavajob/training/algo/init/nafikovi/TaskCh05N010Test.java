package com.getjavajob.training.algo.init.nafikovi;

import java.util.Arrays;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh05N010.calculateUsdToRub;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N010Test {
    public static void main(String[] args) {
        usdRubTest();
    }

    public static void usdRubTest() {
        double[] testArray = new double[20];
        for (int i = 0; i < 20; i++) {
            testArray[i] = (i + 1) * 10;
        }
        assertEquals("TaskCh05N010Test.usdRubTest", true, Arrays.equals(testArray, calculateUsdToRub(10)));
    }
}
