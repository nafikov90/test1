package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh06N087.Game;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh06N087Test {
    public static void main(String[] args) {
        testIntermediateScore();
        testResult();
    }

    public static void testIntermediateScore() {
        Game testGame = new Game("A", "B");
        String trueScore = "A 2 : 0 B";
        assertEquals("TaskCh06N087Test.intermediateScore", true,
                testGame.intermediateScore(1, 2).equals(trueScore));
    }

    public static void testResult() {
        Game testGame = new Game("A", "B");
        testGame.intermediateScore(1, 3);
        testGame.intermediateScore(2, 1);
        testGame.intermediateScore(1, 2);
        testGame.intermediateScore(2, 2);
        String trueResult = "A 5 : 3 B";
        assertEquals("TaskCh06N087Test.testResult", true, testGame.result().equals(trueResult));
    }
}
