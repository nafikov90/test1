package com.getjavajob.training.algo.init.nafikovi;

import java.util.Random;

public class TaskCh12N063 {
    private static final int COUNT_OF_CLASSES = 11;

    public static void main(String[] args) {
        int[][] school = initSchoolClass();
        printArray(school);
        double[] result = averageClass(school);
        for (double d : result) {
            System.out.print(d + " ");
        }
    }

    public static int[][] initSchoolClass() {
        int[][] school = new int[11][4];
        Random random = new Random();
        for (int i = 0; i < COUNT_OF_CLASSES; i++) {
            for (int j = 0; j < 4; j++) {
                school[i][j] = random.nextInt(15) + 20;
            }
        }
        return school;
    }

    public static void printArray(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static double[] averageClass(int[][] arr) {
        double[] averResult = new double[COUNT_OF_CLASSES];
        for (int i = 0; i < arr.length; i++) {
            int sum = 0;
            for (int j = 0; j < arr[i].length; j++) {
                sum += arr[i][j];
            }
            averResult[i] = sum * 1.0 / arr[i].length;
        }
        return averResult;
    }
}
