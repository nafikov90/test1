package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh02N039 {
    private static final int ANGLES_PER_HOUR = 30;
    private static final int HALF_OF_DAY = 12;
    private static final double ANGLES_PER_MINUTE = 0.5;
    private static final double ANGLES_PER_SECOND = 1.0 / 120;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the hours (0 - 23)");
        int hour = scanner.nextInt();
        System.out.println("Enter the minutes (0 - 59)");
        int minute = scanner.nextInt();
        System.out.println("Enter the seconds (0 - 59)");
        int second = scanner.nextInt();
        double result = calculateAngle(hour, minute, second);
        System.out.println("Angle between pointer: " + result);
    }

    public static double calculateAngle(int hour, int minute, int second) {
        if (hour >= HALF_OF_DAY) {
            hour -= HALF_OF_DAY;
        }
        return hour * ANGLES_PER_HOUR + minute * ANGLES_PER_MINUTE + second * ANGLES_PER_SECOND;
    }
}
