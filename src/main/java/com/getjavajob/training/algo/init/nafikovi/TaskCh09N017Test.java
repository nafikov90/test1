package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh09N017.isMatchFirstAndlast;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N017Test {
    public static void main(String[] args) {
        testMatch1();
        testMatch2();
    }

    public static void testMatch1() {
        assertEquals("TaskCh09N017Test.testMatch1", true, isMatchFirstAndlast("alpha"));
    }

    public static void testMatch2() {
        assertEquals("TaskCh09N017Test.testMatch2", false, isMatchFirstAndlast("algo"));
    }
}
