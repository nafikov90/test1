package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh06N087 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter the name 1 team");
        String team1 = scanner.nextLine();
        System.out.println("Enter the name 2 team");
        String team2 = scanner.nextLine();
        Game game = new Game(team1, team2);
        game.play();
    }

    public static class Game {
        private String team1;
        private String team2;
        private int score1;
        private int score2;

        public Game(String team1, String team2) {
            this.team1 = team1;
            this.team2 = team2;
            this.score1 = 0;
            this.score2 = 0;
        }

        public void play() {
            System.out.println("Game started!!!");
            System.out.println(team1 + " " + score1 + " : " + score2 + " " + team2);
            int changeTeam;

            while (true) {
                System.out.println("Enter team to score (1 or 2 or 0 to finish game):");
                changeTeam = scanner.nextInt();
                if (changeTeam == 0) {
                    break;
                }
                System.out.println("Enter score (1 or 2 or 3):");
                int scoreCurrent = scanner.nextInt();
                System.out.println(intermediateScore(changeTeam, scoreCurrent));
            }
            System.out.println("End of the game");
            System.out.println(result());
        }

        public String intermediateScore(int changeTeam, int scoreCurrent) {
            if (scoreCurrent > 3 || scoreCurrent < 1) {
                System.out.println("error input");
                return team1 + " " + score1 + " : " + score2 + " " + team2;
            } else if (changeTeam == 1) {
                score1 += scoreCurrent;
            } else if (changeTeam == 2) {
                score2 += scoreCurrent;
            } else {
                System.out.println("error input");
            }
            return team1 + " " + score1 + " : " + score2 + " " + team2;
        }

        public String result() {
            return team1 + " " + score1 + " : " + score2 + " " + team2;
        }
    }
}
