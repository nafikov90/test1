package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh10N047 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of the Fibonacci Sequence");
        int k = scanner.nextInt();
        int fibo = getFiboN(k);
        System.out.println(fibo);
    }

    public static int getFiboN(int k) {
        return k < 3 ? 1 : getFiboN(k - 1) + getFiboN(k - 2);
    }
}
