package com.getjavajob.training.algo.init.nafikovi;

import java.util.ArrayList;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh13N012.Database;
import static com.getjavajob.training.algo.init.nafikovi.TaskCh13N012.Employee;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh13N012Test {

    public static void main(String[] args) {
        testFindEmployeesByName();
        testfindEmployeesByExperience();
    }

    public static void testFindEmployeesByName() {
        Database db = new Database();
        db.initDatabase();
        ArrayList<Employee> expected = new ArrayList<>();
        expected.add(new Employee("Petr", "Ivanov", "Moscow, Tverskaya 12", "11.2008"));
        expected.add(new Employee("Igor", "Ivanov", "Ufa,  Lenina, 33", "10.2013"));
        expected.add(new Employee("Olga", "Tikhonova", "Ivanovna", "Tula,  Pirogova 12", "05.2012"));
        expected.add(new Employee("Ivan", "Prudnikov", "Tomsk,  Lenina, 49", "09.2009"));
        assertEquals("TaskCh13N012Test.testFindEmployeesByName", expected, db.findEmployeesByName("iVaN"));
    }

    public static void testfindEmployeesByExperience() {
        Database db = new Database();
        db.initDatabase();
        ArrayList<Employee> expected = new ArrayList<>();
        expected.add(new Employee("Alina", "Kupcova", "Igorevna", "Omsk,  Lenina, 48", "01.2001"));
        expected.add(new Employee("Bill", "Murrey", "Los Angeles,  Lenina, 15", "01.1998"));
        expected.add(new Employee("Wayne", "Rooney", "Manchester, Lenina, 44", "05.2003"));
        assertEquals("TaskCh13N012Test.testfindEmployeesByExperience",
                expected, db.findEmployeesByExperience(10));
    }
}
