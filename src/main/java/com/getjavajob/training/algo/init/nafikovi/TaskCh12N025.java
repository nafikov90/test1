package com.getjavajob.training.algo.init.nafikovi;

public class TaskCh12N025 {
    public static void main(String[] args) {
        printArray(fillTheArrayA());
        printArray(fillTheArrayB());
        printArray(fillTheArrayC());
        printArray(fillTheArrayD());
        printArray(fillTheArrayE());
        printArray(fillTheArrayF());
        printArray(fillTheArrayG());
        printArray(fillTheArrayH());
        printArray(fillTheArrayI());
        printArray(fillTheArrayJ());
        printArray(fillTheArrayK());
        printArray(fillTheArrayL());
        printArray(fillTheArrayM());
        printArray(fillTheArrayN());
        printArray(fillTheArrayO());
        printArray(fillTheArrayP());
    }

    public static int[][] fillTheArrayA() {
        int[][] arr = new int[12][10];
        int a = 1;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                arr[i][j] = a;
                a++;
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayB() {
        int[][] arr = new int[12][10];
        int a = 1;
        for (int j = 0; j < arr[0].length; j++) {
            for (int i = 0; i < arr.length; i++) {
                arr[i][j] = a;
                a++;
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayC() {
        int[][] arr = new int[12][10];
        int a = 1;
        for (int i = 0; i < arr.length; i++) {
            for (int j = arr[0].length - 1; j >= 0; j--) {
                arr[i][j] = a;
                a++;
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayD() {
        int[][] arr = new int[12][10];
        int a = 1;
        for (int j = 0; j < arr[0].length; j++) {
            for (int i = arr.length - 1; i >= 0; i--) {
                arr[i][j] = a;
                a++;
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayE() {
        int[][] arr = new int[10][12];
        int a = 1;
        for (int i = 0; i < arr.length; i++) {
            if ((a / 12) % 2 == 0) {
                for (int j = 0; j < arr[0].length; j++) {
                    arr[i][j] = a;
                    a++;
                }
            } else {
                for (int j = arr[0].length - 1; j >= 0; j--) {
                    arr[i][j] = a;
                    a++;
                }
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayF() {
        int[][] arr = new int[12][10];
        int a = 1;
        for (int j = 0; j < arr[0].length; j++) {
            if ((a / 12) % 2 == 0) {
                for (int i = 0; i < arr.length; i++) {
                    arr[i][j] = a;
                    a++;
                }
            } else {
                for (int i = arr.length - 1; i >= 0; i--) {
                    arr[i][j] = a;
                    a++;
                }
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayG() {
        int[][] arr = new int[12][10];
        int a = 1;
        for (int i = arr.length - 1; i >= 0; i--) {
            for (int j = 0; j < arr[0].length; j++) {
                arr[i][j] = a;
                a++;
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayH() {
        int[][] arr = new int[12][10];
        int a = 1;
        for (int j = arr[0].length - 1; j >= 0; j--) {
            for (int i = 0; i < arr.length; i++) {
                arr[i][j] = a;
                a++;
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayI() {
        int[][] arr = new int[12][10];
        int a = 1;
        for (int i = arr.length - 1; i >= 0; i--) {
            for (int j = arr[0].length - 1; j >= 0; j--) {
                arr[i][j] = a;
                a++;
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayJ() {
        int[][] arr = new int[12][10];
        int a = 1;
        for (int j = arr[0].length - 1; j >= 0; j--) {
            for (int i = arr.length - 1; i >= 0; i--) {
                arr[i][j] = a;
                a++;
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayK() {
        int[][] arr = new int[12][10];
        int a = 1;
        for (int i = arr.length - 1; i >= 0; i--) {
            if ((a / 10) % 2 == 0) {
                for (int j = 0; j < arr[0].length; j++) {
                    arr[i][j] = a;
                    a++;
                }
            } else {
                for (int j = arr[0].length - 1; j >= 0; j--) {
                    arr[i][j] = a;
                    a++;
                }
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayL() {
        int[][] arr = new int[12][10];
        int a = 1;
        for (int i = 0; i < arr.length; i++) {
            if ((a / 10) % 2 == 1) {
                for (int j = 0; j < arr[0].length; j++) {
                    arr[i][j] = a;
                    a++;
                }
            } else {
                for (int j = arr[0].length - 1; j >= 0; j--) {
                    arr[i][j] = a;
                    a++;
                }
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayM() {
        int[][] arr = new int[12][10];
        int a = 1;
        for (int j = arr[0].length - 1; j >= 0; j--) {
            if ((a / 12) % 2 == 0) {
                for (int i = 0; i < arr.length; i++) {
                    arr[i][j] = a;
                    a++;
                }
            } else {
                for (int i = arr.length - 1; i >= 0; i--) {
                    arr[i][j] = a;
                    a++;
                }
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayN() {
        int[][] arr = new int[12][10];
        int a = 1;
        for (int j = 0; j < arr[0].length; j++) {
            if ((a / 12) % 2 == 1) {
                for (int i = 0; i < arr.length; i++) {
                    arr[i][j] = a;
                    a++;
                }
            } else {
                for (int i = arr.length - 1; i >= 0; i--) {
                    arr[i][j] = a;
                    a++;
                }
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayO() {
        int[][] arr = new int[12][10];
        int a = 1;
        for (int i = arr.length - 1; i >= 0; i--) {
            if ((a / 10) % 2 == 1) {
                for (int j = 0; j < arr[0].length; j++) {
                    arr[i][j] = a;
                    a++;
                }
            } else {
                for (int j = arr[0].length - 1; j >= 0; j--) {
                    arr[i][j] = a;
                    a++;
                }
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayP() {
        int[][] arr = new int[12][10];
        int a = 1;
        for (int j = arr[0].length - 1; j >= 0; j--) {
            if ((a / 12) % 2 == 0) {
                for (int i = arr.length - 1; i >= 0; i--) {
                    arr[i][j] = a;
                    a++;
                }
            } else {
                for (int i = 0; i < arr.length; i++) {
                    arr[i][j] = a;
                    a++;
                }
            }
        }
        return arr;
    }

    public static void printArray(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
