package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh09N022 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the word with an even number of letters");
        String word = scanner.nextLine();
        String result = halfOfWord(word);
        System.out.println(result);
    }

    public static String halfOfWord(String word) {
        return word.substring(0, word.length() / 2);
    }
}
