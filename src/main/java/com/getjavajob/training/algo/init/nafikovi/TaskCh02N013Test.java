package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh02N013.reverseNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;


public class TaskCh02N013Test {
    public static void main(String[] args) {
        testReverse();
    }

    public static void testReverse() {
        assertEquals("TaskCh02N013Test.testReverse", 321, reverseNumber(123));
    }
}
