package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh09N042 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the word");
        String word = scanner.nextLine();
        String result = reverseWord(word);
        System.out.println(result);
    }

    public static String reverseWord(String word) {
        return new StringBuilder(word).reverse().toString();
    }
}
