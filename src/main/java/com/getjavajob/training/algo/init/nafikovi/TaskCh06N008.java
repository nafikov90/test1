package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh06N008 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number N");
        int[] result = calculate(scanner.nextInt());
        printResult(result);
    }

    public static int[] calculate(int n) {
        int numberOfElements = (int) Math.floor(Math.sqrt(n));
        int[] array = new int[numberOfElements];
        for (int i = 0; i < numberOfElements; i++) {
            array[i] = (i + 1) * (i + 1);
        }
        return array;
    }

    public static void printResult(int[] result) {
        for (int a : result) {
            System.out.println(a);
        }
    }
}
