package com.getjavajob.training.algo.init.nafikovi;

import java.util.Arrays;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh12N234.deleteColumn;
import static com.getjavajob.training.algo.init.nafikovi.TaskCh12N234.deleteLine;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N234Test {
    public static void main(String[] args) {
        testDeleteLine();
        testDeleteColumn();
    }

    public static void testDeleteLine() {
        int[][] testArrayBefore = new int[][]{
                {1, 1, 1, 1},
                {2, 2, 2, 2},
                {3, 3, 3, 3},
                {4, 4, 4, 4},
                {5, 5, 5, 5}
        };
        int[][] testArrayAfter = new int[][]{
                {1, 1, 1, 1},
                {2, 2, 2, 2},
                {4, 4, 4, 4},
                {5, 5, 5, 5},
                {0, 0, 0, 0}
        };
        assertEquals("TaskCh12N234Test.testDeleteLine", true,
                Arrays.deepEquals(testArrayAfter, deleteLine(testArrayBefore, 3)));
    }

    public static void testDeleteColumn() {
        int[][] testArrayBefore = new int[][]{
                {1, 2, 3, 4},
                {1, 2, 3, 4},
                {1, 2, 3, 4},
                {1, 2, 3, 4},
                {1, 2, 3, 4}
        };
        int[][] testArrayAfter = new int[][]{
                {1, 3, 4, 0},
                {1, 3, 4, 0},
                {1, 3, 4, 0},
                {1, 3, 4, 0},
                {1, 3, 4, 0}
        };
        assertEquals("TaskCh12N234Test.testDeleteColumn", true, Arrays.deepEquals(testArrayAfter,
                deleteColumn(testArrayBefore, 2)));
    }
}
