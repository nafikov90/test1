package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N041.factorial;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N041Test {
    public static void main(String[] args) {
        testFact1();
        testFact2();
    }

    public static void testFact1() {
        assertEquals("TaskCh10N041Test.testFact1", 1, factorial(1));
    }

    public static void testFact2() {
        assertEquals("TaskCh10N041Test.testFact2", 24, factorial(4));
    }
}
