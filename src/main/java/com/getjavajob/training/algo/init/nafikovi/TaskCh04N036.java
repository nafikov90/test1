package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh04N036 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of minutes");
        int minutes = scanner.nextInt();
        String result = colorSignal(minutes);
        System.out.println(result);
    }

    public static String colorSignal(int minutes) {
        return minutes % 5 >= 3 ? "Red" : "Green";
    }
}
