package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N050.calcAck;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N050Test {
    public static void main(String[] args) {
        testAck1();
        testAck2();
        testAck3();
    }

    public static void testAck1() {
        assertEquals("TaskCh10N050Test.testAck1", 4, calcAck(0, 3));
    }

    public static void testAck2() {
        assertEquals("TaskCh10N050Test.testAck2", 3, calcAck(2, 0));
    }

    public static void testAck3() {
        assertEquals("TaskCh10N050Test.testAck3", 125, calcAck(3, 4));
    }
}
