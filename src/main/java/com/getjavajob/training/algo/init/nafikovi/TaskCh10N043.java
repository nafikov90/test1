package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh10N043 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the natural number a");
        int a = scanner.nextInt();
        int sumOfDigit = calcSumOfDigit(a);
        System.out.println("The sum of the number of digits: " + sumOfDigit);
        int countOfDigit = calcCountOfDigit(a);
        System.out.println("The count of digits in the number of: " + countOfDigit);
    }

    public static int calcSumOfDigit(int a) {
        if (a / 10 == 0) {
            return a % 10;
        }
        return a % 10 + calcSumOfDigit(a / 10);
    }

    public static int calcCountOfDigit(int a) {
        if (a / 10 == 0) {
            return 1;
        }
        return 1 + calcCountOfDigit(a / 10);
    }
}
