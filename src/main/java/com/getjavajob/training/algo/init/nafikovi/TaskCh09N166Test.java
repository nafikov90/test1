package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh09N166.replaceWords;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N166Test {
    public static void main(String[] args) {
        testReplace();
    }

    public static void testReplace() {
        assertEquals("TaskCh09N166Test.testReplace", true,
                replaceWords("moscow minsk london").equals("london minsk moscow"));
    }
}
