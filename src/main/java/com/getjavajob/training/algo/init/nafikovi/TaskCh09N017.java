package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh09N017 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the word");
        String word = scanner.nextLine();
        boolean result = isMatchFirstAndlast(word);
        System.out.println(result);
    }

    public static boolean isMatchFirstAndlast(String word) {
        return word.charAt(0) == word.charAt(word.length() - 1);
    }
}