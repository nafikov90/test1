package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh04N115 {
    private static final int ZERO_YEAR = 4;
    private static final int LENGTH_LOOP = 60;
    private static final int ANIMAL_LOOP = 12;
    private static final int COLOR_LOOP = 10;
    private static final int CURRENCY_COLOR = 2;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of years");
        int numberOfYears = scanner.nextInt();
        String result = animalAndColorIs(numberOfYears);
        System.out.println(result);
    }

    public static String animalAndColorIs(int numberOfYears) {
        String animal;
        int yearIn12YearCicle = (numberOfYears - ZERO_YEAR) % LENGTH_LOOP % ANIMAL_LOOP;
        switch (yearIn12YearCicle) {
            case 0:
                animal = "Rat";
                break;
            case 1:
                animal = "Cow";
                break;
            case 2:
                animal = "Tiger";
                break;
            case 3:
                animal = "Hare";
                break;
            case 4:
                animal = "Dragon";
                break;
            case 5:
                animal = "Snake";
                break;
            case 6:
                animal = "Horse";
                break;
            case 7:
                animal = "Sheep";
                break;
            case 8:
                animal = "Monkey";
                break;
            case 9:
                animal = "Cock";
                break;
            case 10:
                animal = "Dog";
                break;
            case 11:
                animal = "Pig";
                break;
            default:
                animal = "Input Error";
                break;
        }

        String color;
        int yearInColorCicle = (numberOfYears - ZERO_YEAR) % COLOR_LOOP / CURRENCY_COLOR;
        switch (yearInColorCicle) {
            case 0:
                color = "Green";
                break;
            case 1:
                color = "Red";
                break;
            case 2:
                color = "Yellow";
                break;
            case 3:
                color = "White";
                break;
            case 4:
                color = "Black";
                break;
            default:
                color = "Input Error";
                break;
        }
        return animal + ", " + color;
    }
}
