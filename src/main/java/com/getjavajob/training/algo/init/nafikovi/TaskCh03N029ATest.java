package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh03N029A.isTrue;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh03N029ATest {
    public static void main(String[] args) {
        test1();
        test2();
        test3();
    }

    public static void test1() {
        assertEquals("TaskCh03N029ATest.test1", true, isTrue(1, 3));
    }

    public static void test2() {
        assertEquals("TaskCh03N029ATest.test2", false, isTrue(1, 2));
    }

    public static void test3() {
        assertEquals("TaskCh03N029ATest.test3", false, isTrue(4, 6));
    }
}
