package com.getjavajob.training.algo.init.nafikovi;

public class TaskCh10N051 {
    public static void main(String[] args) {
        calculateA(5);
        System.out.println();
        calculateB(5);
        System.out.println();
        calculateC(5);
    }

    public static void calculateA(int n) {
        if (n > 0) {
            System.out.print(n + " ");
            calculateA(n - 1);
        }
    }

    public static void calculateB(int n) {
        if (n > 0) {
            calculateB(n - 1);
            System.out.print(n + " ");
        }
    }

    public static void calculateC(int n) {
        if (n > 0) {
            System.out.print(n + " ");
            calculateC(n - 1);
            System.out.print(n + " ");
        }
    }
}
