package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N052.resetValueOfString;
import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N052.reversePrint;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N052Test {
    public static void main(String[] args) {
        testReverse1();
        testReverse2();
        testReverse3();
    }

    public static void testReverse1() {
        resetValueOfString();
        assertEquals("TaskCh10N052Test.testReverse1", 12, reversePrint(21));
    }

    public static void testReverse2() {
        resetValueOfString();
        assertEquals("TaskCh10N052Test.testReverse2", 1, reversePrint(100));
    }

    public static void testReverse3() {
        resetValueOfString();
        assertEquals("TaskCh10N052Test.testReverse3", 10009, reversePrint(90001));
    }


}
