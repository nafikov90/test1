package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh02N013 {
    public static void main(String[] args) {
        System.out.println("Enter the number from 100 to 200");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int result = reverseNumber(number);
        System.out.println(result);
    }

    public static int reverseNumber(int number) {
        return number % 10 * 100 + (number / 10) % 10 * 10 + number / 100;
    }
}



